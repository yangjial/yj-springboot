package com.ft.modules.sys.service.impl;

import com.ft.modules.sys.model.Oder;
import com.ft.modules.sys.mapper.OderMapper;
import com.ft.modules.sys.service.OderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author YangJia
 * @since 2021-07-28
 */
@Service
public class OderServiceImpl extends ServiceImpl<OderMapper, Oder> implements OderService {

}
