package com.ft.modules.sys.service.impl;

import com.ft.modules.sys.model.TUser;
import com.ft.modules.sys.mapper.TUserMapper;
import com.ft.modules.sys.service.TUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表	 服务实现类
 * </p>
 *
 * @author YangJia
 * @since 2021-07-21
 */
@Service
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements TUserService {

}
