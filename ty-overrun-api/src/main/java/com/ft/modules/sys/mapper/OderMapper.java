package com.ft.modules.sys.mapper;

import com.ft.modules.sys.model.Oder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author YangJia
 * @since 2021-07-28
 */
public interface OderMapper extends BaseMapper<Oder> {

}
