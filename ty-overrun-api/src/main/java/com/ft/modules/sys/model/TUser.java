package com.ft.modules.sys.model;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户表	
 * </p>
 *
 * @author YangJia
 * @since 2021-07-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="TUser对象", description="用户表")
public class TUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标识")
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "??")
    @TableField("POSITION")
    private Integer position;

    @ApiModelProperty(value = "别名")
    @TableField("ALIAS")
    private String alias;

    @ApiModelProperty(value = "密码")
    @TableField("PASSWORD")
    private String password;

    @ApiModelProperty(value = "姓名")
    @TableField("NAME")
    private String name;

    @ApiModelProperty(value = "昵称")
    @TableField("NICKNAME")
    private String nickname;

    @ApiModelProperty(value = "组织")
    @TableField("ORGANIZATION")
    private Integer organization;

    @ApiModelProperty(value = "证书编号")
    @TableField("CERTIFICATE_NO")
    private String certificateNo;

    @ApiModelProperty(value = "证书名称")
    @TableField("CERTIFICATE_CLASS")
    private String certificateClass;

    @ApiModelProperty(value = "取得证书日期")
    @TableField("CERTIFICATE_DATE")
    private Date certificateDate;

    @ApiModelProperty(value = "取得证书日期")
    @TableField("VALID_DATE")
    private Date validDate;

    @ApiModelProperty(value = "状态，0：停用，1：启用")
    @TableField("STATUS")
    private Integer status;

    @ApiModelProperty(value = "描述")
    @TableField("DESCRIPTION")
    private String description;

    @ApiModelProperty(value = "职务")
    @TableField("POST")
    private String post;

    @ApiModelProperty(value = "是否邮件接收者")
    @TableField("IS_MAIL_RECEIVE")
    private Integer isMailReceive;

    @ApiModelProperty(value = "状态，0：案件办理主题，1：无主题2：统计分析主题")
    @TableField("THEME")
    private Integer theme;

    @ApiModelProperty(value = "用户微信OpenID")
    @TableField("OPENID")
    private String openid;

    @ApiModelProperty(value = "用于标识元善用户添加")
    @TableField("BACKUP1")
    private String backup1;

    @ApiModelProperty(value = "添加时间")
    @TableField("ADD_TIME")
    private Date addTime;

    @ApiModelProperty(value = "专管类型")
    @TableField("CHARGE_TYPE")
    private String chargeType;

    @ApiModelProperty(value = "手机号码")
    @TableField("TELEPHONE")
    private String telephone;

    private Integer hyId;


}
