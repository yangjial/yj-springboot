package com.ft.modules.sys.controller;

import com.ft.common.domain.ResponseInfo;
import com.ft.modules.sys.model.TUser;
import com.ft.modules.sys.service.TUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户表	 前端控制器
 * </p>
 *
 * @author YangJia
 * @since 2021-07-21
 */
@RestController
@RequestMapping("/t-user")
@Api(tags = "用户相关接口")
public class TUserController {
    protected static Logger log = LoggerFactory.getLogger(TUserController.class);

    @Autowired
    TUserService tUserService;

    @ApiOperation(value = "根据Id查询用户信息",consumes = "application/json;charset=UTF-8")
    @RequestMapping(value = "/getUserInfoById", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="userId",value="用户Id",required=true,paramType="form",dataType="Integer"),
    })
    public ResponseEntity<ResponseInfo<TUser>> getUserInfoById(Integer userId){
        ResponseInfo ri = new ResponseInfo();
        TUser user = tUserService.getById(userId);
        ri.setData(user);
        return ResponseEntity.ok().body(ri.ok());
    }
}
