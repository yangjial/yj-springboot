package com.ft.modules.sys.mapper;

import com.ft.modules.sys.model.TUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表	 Mapper 接口
 * </p>
 *
 * @author YangJia
 * @since 2021-07-21
 */
public interface TUserMapper extends BaseMapper<TUser> {

}
