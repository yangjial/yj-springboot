package com.ft.modules.sys.service;

import com.ft.modules.sys.model.Oder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author YangJia
 * @since 2021-07-28
 */
public interface OderService extends IService<Oder> {

}
