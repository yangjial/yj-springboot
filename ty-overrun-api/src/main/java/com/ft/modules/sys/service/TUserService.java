package com.ft.modules.sys.service;

import com.ft.modules.sys.model.TUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表	 服务类
 * </p>
 *
 * @author YangJia
 * @since 2021-07-21
 */
public interface TUserService extends IService<TUser> {

}
