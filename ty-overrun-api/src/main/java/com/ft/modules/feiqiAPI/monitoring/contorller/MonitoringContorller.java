package com.ft.modules.feiqiAPI.monitoring.contorller;

import com.alibaba.fastjson.JSONObject;
import com.ft.common.domain.ResponseInfo;
import com.ft.modules.feiqiAPI.config.FQApiHttpUtil;
import com.ft.modules.sys.service.TUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  飞企接口--视频监控系统
 * </p>
 *
 * @author YangJia
 * @since 2021-07-29
 */
@RestController
@RequestMapping("/Frontop_FQ_API_Monitoring")
@Api(tags = "一、视频监控系统")
public class MonitoringContorller {
    @Autowired
    TUserService tUserService;
    @ApiOperation(value = "视频监控系统相关API汇总",consumes = "application/json;charset=UTF-8")
    @RequestMapping(value = "/apiGather", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="methodName",value="方法名：" +
                    "1、pandect（摄像头数量汇总）" +
                    "2、runStatus（摄像头运行状态）" +
                    "3、faultStatis（设备警告/故障情况）" +
                    "4、device（实时监控）",required=true,paramType="form",dataType="Integer"),
    })
    public ResponseEntity<ResponseInfo<JSONObject>> apiGather(String methodName){
        ResponseInfo ri = new ResponseInfo();
        JSONObject params = new JSONObject();
        params.put("methodName",methodName);
        JSONObject jsonObject = FQApiHttpUtil.request(params,"/iot/ft/dh/entrance",false);
        ri.setData(jsonObject);
        return ResponseEntity.ok().body(ri.ok());
    }

}