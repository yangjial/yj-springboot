package com.ft.modules.feiqiAPI.config;


import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * <p>
 *  飞企接口--请求公共类
 * </p>
 *
 * @author YangJia
 * @since 2021-07-29
 */
public class FQApiHttpUtil {
    //Http前缀
    public static final String httpUrlPrefix = "http://47.107.107.161:80";
    //登录账号
    public static final String fqAccount = "2029439301";
    //登录密码
    public static final String fqPassword = "26DAB781FF0CD4854AC920B4905231BB";
    //登录成功后的token
    public static String token = null;

    /**
     * 请求
     * @param js 参数
     * @param url Url后缀
     * @param isLogin 是否是登录请求
     * @return JSONObject 结果
     */
    public static JSONObject request(JSONObject js, String url,boolean isLogin){
        CloseableHttpClient httpClient = null;
        HttpPost httpPost = new HttpPost(httpUrlPrefix+url);
        try {
            httpClient = HttpClients.createDefault();
            // 设置请求参数
            if(js != null){
                httpPost.setEntity(new StringEntity(js.toJSONString(), "utf-8"));
            }
            // 设置header信息
            if(!isLogin){
                if(StringUtils.isEmpty(token)){
                    loginFq();
                }
                httpPost.setHeader("FT-Token", token);
            }
            httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
            httpPost.setHeader("User-Agent",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0");
            // 执行请求
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity resEntity = httpResponse.getEntity();
            // 按指定编码转换结果实体为String类型
            String result = EntityUtils.toString(resEntity, "utf-8");
            return JSONObject.parseObject(result);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * 登录
     */
    public static void loginFq(){
        JSONObject parm = new JSONObject();
        parm.put("account",fqAccount);
        parm.put("password",fqPassword);
        JSONObject jsonObject = request(parm,"/iot/ft/user/login",true);
        token = jsonObject.getString("data");
        System.out.println("飞企API登录成功："+token);
    }

}
