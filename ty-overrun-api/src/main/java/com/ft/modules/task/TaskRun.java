package com.ft.modules.task;

import com.ft.modules.feiqiAPI.config.FQApiHttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务调度
 * @author YangJia
 */
@Component
@EnableScheduling
@Async
//默认条件注解是开启的，现在采用配置文件的变量来手动控制定时任务是否执行
@ConditionalOnProperty(prefix = "scheduling", name = "enabled", havingValue = "true")
@PropertySource("classpath:application.yml")
public class TaskRun {
    Logger logger = LoggerFactory.getLogger(TaskRun.class);
    /**
     * 每30分钟更新一次飞企的token
     */
    @Scheduled(cron = "0 01,31 * * * ? ")
    public void syncVehicle() {
        FQApiHttpUtil.loginFq();
    }

}
