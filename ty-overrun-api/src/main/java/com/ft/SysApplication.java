package com.ft;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
@EnableAsync
@SpringBootApplication
@EnableScheduling//启动定时任务配置
@MapperScan("com.ft.modules.*.mapper**")
public class SysApplication {

	public static void main(String[] args) {
		SpringApplication.run(SysApplication.class, args);
		//
		System.out.println("----------------start-success------------------");

	}

}
