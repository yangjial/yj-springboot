package com.ft.common.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.ft.common.json.JsonHelper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.poi.ss.formula.functions.T;

/**
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
@ApiModel(description= "返回响应数据")
public class ResponseInfo<T> implements Serializable {
	@ApiModelProperty(value = "状态码")
	private String code;
	@ApiModelProperty(value = "响应消息")
	private String msg;
	private Integer uType = 1;
	@ApiModelProperty(value = "响应数据")
	private T data;
	
	public ResponseInfo() {}

	public ResponseInfo(Integer uType) {
		this.uType = uType;
	}
	

	
	public ResponseInfo(String code,String msg) {
		this.code = code;
		this.msg = msg;
	}
	

	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public ResponseInfo ok() {
		this.ok(null);
		return this;
	}
	
	public ResponseInfo bad() {
		this.bad(null);
		return this;
	}
	
	public ResponseInfo setError(String code,String error) {
		this.code = code;
		if(uType == 1 && ("400".equals(code)|| "500".equals(code))) {
			this.msg = "系统异常！请联系管理员！";
		}else if(uType == 1 && "403".equals(code)){
			this.msg = "您沒有权限进行操作！请联系管理员！";
		}else {
			this.msg = error;
		}
		return this;
	}
	
	public ResponseInfo ok(String msg) {
		this.code = "1";
		this.msg = msg == null ? "请求成功" : msg;
		return this;
	}
	
	public ResponseInfo bad(String msg) {
		this.code = "0";
		this.msg = msg == null ? "请求异常" : msg;
		return this;
	}
	
	public ResponseInfo success() {
		this.success(null);
		return this;
	}
	
	public ResponseInfo failed() {
		this.failed(null);
		return this;
	}
	
	public ResponseInfo success(String msg) {
		this.code = "0200";
		this.msg = msg == null ? "操作成功" : msg;
		return this;
	}
	
	public ResponseInfo failed(String msg) {
		this.code = "0100";
		this.msg = msg == null ? "操作失败" : msg;
		return this;
	}
	
	public void logout() {
		this.code = "1001";
		this.msg = "登陆状态失效";
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	public Integer getuType() {
		return uType;
	}

	public void setuType(Integer uType) {
		this.uType = uType;
	}

	@Override
	public String toString() {
		return JsonHelper.parseToJson(this, null);
	}

	
}
