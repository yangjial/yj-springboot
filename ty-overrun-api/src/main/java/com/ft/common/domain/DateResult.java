package com.ft.common.domain;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description= "返回响应数据")
public class DateResult<T> implements Serializable {
    @ApiModelProperty(value = "状态码")
    private String code;
    @ApiModelProperty(value = "响应数据")
    private T result;
    @ApiModelProperty(value = "响应消息")
    private String msg;

    public DateResult() {
    }
    public DateResult(T result, String code, String msg) {
        this.result = result;
        this.code = code;
        this.msg = msg;
    }

    public T getResult() {
        return (T) result;
    }

    public void setResult(T result) {
        this.result = result;
    }


    public String getCode() {
        return code;
    }



    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }



    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
