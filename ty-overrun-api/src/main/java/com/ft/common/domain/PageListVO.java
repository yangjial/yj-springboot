package com.ft.common.domain;

import java.util.List;

/**
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
public class PageListVO<T> {

	private Long total;
	private List<T> rows;
	
	public PageListVO() {}
	
	public PageListVO(Long total,List<T> rows){
		this.total = total;
		this.rows = rows;
	}
	
	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

	@Override
	public String toString() {
		return "PageDO{" + ", total=" + total + ", rows=" + rows + '}';
	}
}
