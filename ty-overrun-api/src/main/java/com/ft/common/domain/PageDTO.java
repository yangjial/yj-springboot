package com.ft.common.domain;

/**
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
public class PageDTO<T> {

	private static final long serialVersionUID = -3326666222298445466L;

	private int pageSize = 10;
	
	private Long total;
	
	private int pageNo = 1;
	
	//0name,1age 0=asc,1=desc
	private String order;
	
	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public String[] desc(Integer i) {
		return new String[i];
	}
	
	public String[] asc(Integer i) {
		return new String[i];
	}
	
}
