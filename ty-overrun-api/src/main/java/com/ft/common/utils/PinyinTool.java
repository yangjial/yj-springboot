package com.ft.common.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/**
 * 汉字拼音工具类
 * 
 * @author Comit
 *
 */
public class PinyinTool {
	HanyuPinyinOutputFormat format = null;

	public static enum Type {
		UPPERCASE, // 全部大写
		LOWERCASE, // 全部小写
		FIRSTUPPER// 首字母大写    
	}

	public PinyinTool() {
		format = new HanyuPinyinOutputFormat();
		format.setCaseType(HanyuPinyinCaseType.UPPERCASE);
		format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
	}

	public String toPinYin(String str) throws BadHanyuPinyinOutputFormatCombination {
		return toPinYin(str, "", Type.UPPERCASE,false);
	}

	public String toPinYin(String str, String spera) throws BadHanyuPinyinOutputFormatCombination {
		return toPinYin(str, spera, Type.UPPERCASE,false);
	}

	/**
	 * 
	 *      * 将str转换成拼音，如果不是汉字或者没有对应的拼音，则不作转换
	 * 
	 *      * 如： 明天 转换成 MINGTIAN
	 * 
	 *      * @param str：要转化的汉字
	 * 
	 *      * @param spera：转化结果的分割符
	 * 
	 *      * @return
	 * 
	 *      * @throws BadHanyuPinyinOutputFormatCombination
	 * 
	 *     
	 */
	public String toPinYin(String str, String spera, Type type,boolean isFirst) throws BadHanyuPinyinOutputFormatCombination {

		if (str == null || str.trim().length() == 0) {
			return "";
		}
		if (type == Type.LOWERCASE) {
			format.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		} else {
			format.setCaseType(HanyuPinyinCaseType.UPPERCASE);
		}
		String py = "";
		String temp = "";
		String[] t;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if ((int) c <= 128)
				py += c;
			else {
				t = PinyinHelper.toHanyuPinyinStringArray(c, format);
				if (t == null) {
					py += c;
				} else {
					temp = t[0];
					if(isFirst){
						temp=temp.substring(0,1);
					}
					if (type == Type.FIRSTUPPER)
						temp = t[0].toUpperCase().charAt(0) + temp.substring(1);
					py += temp + (i == str.length() - 1 ? "" : spera);
				}
			}
		}
		return py.trim();
	}
}
