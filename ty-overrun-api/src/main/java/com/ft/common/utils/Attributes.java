package com.ft.common.utils;

/**
 * @remark Yj
 * 地图标点
 */
public class Attributes {
    private Integer ID;
    private String QLDM;
    private String QLMC;
    private String LXBM;
    private String LXMC;
    private String contacts;
    private String phone;
    private String checkId;
    private String cityName;
    private String areaName;
    private boolean isSite;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCheckId() {
        return checkId;
    }

    public void setCheckId(String checkId) {
        this.checkId = checkId;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getQLDM() {
        return QLDM;
    }

    public void setQLDM(String QLDM) {
        this.QLDM = QLDM;
    }

    public String getQLMC() {
        return QLMC;
    }

    public void setQLMC(String QLMC) {
        this.QLMC = QLMC;
    }

    public String getLXBM() {
        return LXBM;
    }

    public void setLXBM(String LXBM) {
        this.LXBM = LXBM;
    }

    public String getLXMC() {
        return LXMC;
    }

    public void setLXMC(String LXMC) {
        this.LXMC = LXMC;
    }

    public boolean isSite() {
        return isSite;
    }

    public void setSite(boolean site) {
        isSite = site;
    }
}
