package com.ft.common.utils;
import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;

public class Base64Utils {
	/**
	 * * BASE64解码
	 *
	 * @param base64
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static byte[] decodeBASE64(String base64) throws UnsupportedEncodingException {
		return Base64.decodeBase64(base64.getBytes("UTF-8"));
	}

	/**
	 * * BASE64解码
	 *
	 * @param base64
	 * @param changeCase
	 * @return
	 */
	public static String decodeBASE64AsString(String base64)throws UnsupportedEncodingException {
		byte[] bytes = decodeBASE64(base64);
		return new String(bytes);
	}

	/**
	 * * BASE64编码
	 *
	 * @param bytes
	 * @return
	 */
	public static String encodeBASE64(byte[] bytes) throws UnsupportedEncodingException {
		byte[] base64Bytes = Base64.encodeBase64(bytes);
		return new String(base64Bytes);
	}

	/**
	 * * BASE64编码
	 *
	 * @param str
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static String encodeBASE64(String str) throws UnsupportedEncodingException {
		return encodeBASE64(str.getBytes("UTF-8"));
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException {
		String s = Base64Utils.encodeBASE64("0360404YWRC");
		System.out.println(s);
		String sb = Base64Utils.decodeBASE64AsString(s);
		System.out.println(sb);
		
//		String s1 = Base64Utils.encodeBASE64("{\"dataId\": \"d309fb50-587a-42d9-a9a8-a50ae0cd0f32\",\"axleArray\":[{\"axleDistance\":0,\"axleNo\":1,\"axleSpeed\": 52.5, \"axleInfo\": 4550 }], \"axleNum\": 1, \"checkId\": \"123\", \"checkTime\": \"2018-01-29 15:15:37\", \"dataIsCredible\": \"0\", \"drivingDirection\": 1, \"imageArray\": [{ \"imageName\": \"car1.jpg\", \"imageUrl\": \"\", \"imageFeature\": \"WD\",\"imageBase64\":\"\"}], \"indexCode\": \"1234\", \"isDangerous\": 0, \"isDirect\": 0, \"isOverWeight\": 1, \"laneNo\": 2, \"limitWeight\": 3586, \"loadRate\": 0.78, \"overWeight\": 2503, \"passCode\": 0, \"plateColor\": 1, \"plateNo\": \"粤C21446\", \"plateType\": 1, \"tyreNums\": 6, \"vehicleColor\": 1, \"vehicleHeight\": 3586, \"vehicleLong\": 11062, \"vehicleSpeed\":80, \"vehicleType\": 3, \"vehicleWeight\": 14800, \"vehicleWidth\": 2503, \"videoArray\": [{ \"videoName\": \"VCG42665736638.mp4\", \"videoUrl\":\"http://localhost:8088/template/VCG42665736638.mp4\"}]}");
		String s1 = Base64Utils.encodeBASE64("{\"dataId\":\"36040467815686229815\",\"indexCode\":\"CQ8V2\",\"checkId\":\"CQ8V\",\"plateNo\":\"赣GV5150\",\"plateColor\":9,\"plateType\":1,\"vehicleColor\":\"Z\",\"vehicleType\":\"X99\",\"vehicleSpeed\":54,\"laneNo\":2,\"isOverWeight\":0,\"axleNum\":6,\"isDirect\":0,\"overWeight\":0.0,\"vehicleWeight\":20920.0,\"limitWeight\":4900.0,\"checkTime\":\"2019-12-31 14:19:50\",\"passCode\":\"229810\",\"axleModel\":\"\",\"axleArray\":[{\"axleNo\":0,\"axleInfo\":0,\"axleDistance\":0,\"axleSpeed\":0}],\"imageArray\":[{\"imageName\":\"20191231141945817_3_2_4_V1.jpg\",\"imageUrl\":\"http://localhost:8088/template/20191231141945817_3_2_4_V1.jpg\",\"imageFeature\":\"D\"},{\"imageName\":\"20191231141945649_3_2_4_PV.jpg\",\"imageUrl\":\"http://localhost:8088/template/20191231141945649_3_2_4_PV.jpg\",\"imageFeature\":\"X\"},{\"imageName\":\"20191231141946026_3_2_4_B1.jpg\",\"imageUrl\":\"http://localhost:8088/template/20191231141946026_3_2_4_B1.jpg\",\"imageFeature\":\"WD\"},{\"imageName\":\"20191231141945373_3_2_4_S1.jpg\",\"imageUrl\":\"http://localhost:8088/template/20191231141945373_3_2_4_S1.jpg\",\"imageFeature\":\"C\"},{\"imageName\":\"20191231141945895_3_2_4_S2.jpg\",\"imageUrl\":\"http://localhost:8088/template/20191231141945895_3_2_4_S2.jpg\",\"imageFeature\":\"S\"},{\"imageName\":\"20191231141945649_3_2_4_BV.jpg\",\"imageUrl\":\"http://localhost:8088/template/20191231141945649_3_2_4_BV.jpg\",\"imageFeature\":\"Q\"}],\"videoArray\":[{\"videoName\":\"20191231141943870_3_2_4_V.mp4\",\"videoUrl\":\"http://localhost:8088/template/20191231141943870_3_2_4_V.mp4\"}],\"dataIsCredible\":\"0\",\"drivingDirection\":\"1\",\"isDangerous\":0,\"tyreNums\":12,\"loadRate\":\"0\",\"vehicleHeight\":0,\"vehicleWidth\":0,\"vehicleLong\":0,\"vehicleBrand\":\"\",\"lengthoverLimitedRate\":\"0.00\",\"widthoverLimitedRate\":\"0.00\",\"heightoverLimitedRate\":\"0.00\",\"lengthoverLimited\":0.0,\"widthoverLimited\":0.0,\"heightoverLimited\":0.0}");
		
//		String s2 = Base64Utils.encodeBASE64("{\"indexCode\": \"001200111\",\"deviceState\": \"0\",\"exceptionReason\": \"摄像头故障\",\"checkTime\": \"2018-01-29 15:15:37\"}");
		System.out.println(s1);
//		System.out.println(s2);
		String sb1 = Base64Utils.decodeBASE64AsString(s1);
		System.out.println(sb1);
		
	}
}