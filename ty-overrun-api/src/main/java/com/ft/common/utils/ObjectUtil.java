package com.ft.common.utils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cglib.beans.BeanMap;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author msi-
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
public class ObjectUtil {
	
	static Logger log = LoggerFactory.getLogger(ObjectUtil.class);
	
	public static String isBlandInObject(Object m,String...keys) {
		if(m == null || keys == null) {
			return null;
		}
		String s = "";
		Object v;
		for(int i=0 ; i<keys.length ; i++) {
			v = getValueFormObject(m, keys[i]);
			if(isEmpty(v)) {
				if("".equals(s)) {
					s += keys[i];
				}else {
					s += ","+ keys[i];
				}
			}
		}
		return s;
	}
	
	public static Boolean checkStrLength(String str, Integer min, Integer max) {
		return (!isEmpty(str) && str.length() >= min && str.length() <= max);
	}
	
	public static String isBlandInMap(Map<String,Object> m,String...keys) {
		
		if(m == null || keys == null) {
			return null;
		}
		String s = "";
		Object v;
		for(int i=0 ; i<keys.length ; i++) {
			v = m.get(keys[i]);
			if(isEmpty(v)) {
				if("".equals(s)) {
					s += keys[i];
				}else {
					s += ","+ keys[i];
				}
			}
		}
		return s;
		
	}
	
	
	
	
	public static Object getValueFormObject(Object object, String fieldName) {
        if (object==null){
        	log.error("the fields is wrong,object is null,fieldName is "+fieldName);
            return null;
        }
        if(fieldName==null||fieldName=="") {
        	log.error("the fields is wrong,object is null,object is  "+object.toString());
            return null;
        }
        Field field;
        try {
            field = object.getClass().getDeclaredField(fieldName);
            if (field != null) {
                field.setAccessible(true);
                return field.get(object);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
        	System.out.println("Get Value Form Object Wrong");
        }
 
        return null;
    }
	
	/**
	 * 非空判断
	 * @param value
	 * @return
	 */
	private static boolean isEmpty(Object value) {
		
		if(value == null) {
			
			return true;
			
		}else if (String.class.isInstance(value)){
			
			return StringUtils.isBlank((String)value);
			
		}else {
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getObjClass(Object obj,Class<T> clazz){
		return (T)obj;
	}
	
    public static Object mapToObject(Map<String, Object> map, Class<?> beanClass) throws Exception {    
        if (map == null)  
            return null;  
  
        Object obj = beanClass.newInstance();  
  
        BeanUtils.populate(obj, map);  
  
        return obj;  
    }    
      
    public static Map<?, ?> objectToMap(Object obj) {  
        if(obj == null)  
            return null;   
  
        return new org.apache.commons.beanutils.BeanMap(obj);
    }  
    
    public static <T> Map<String, Object> beanToMap(T bean,String[] excludes) {
    	Map<String, Object> map = Maps.newHashMap();
        if (bean != null) {
            BeanMap beanMap = BeanMap.create(bean);
            retry:
            for (Object key : beanMap.keySet()) {
            	if(excludes != null && excludes.length != 0) {
            		for(int i=0 ; i<excludes.length ; i++) {
                		if(key.equals(excludes[i])) {
                			continue retry;
                		}
                	}
            	}
            	map.put(key + "", beanMap.get(key));
            }
        }
        return map;
    }
    
    /**
     * 将对象装换为map
     *
     * @param bean
     * @return
     */
    public static <T> Map<String, Object> beanToMap(T bean) {
        return beanToMap(bean,null);
    }

    /**
     * 将map装换为javabean对象
     *
     * @param map
     * @param bean
     * @return
     */
    public static <T> T mapToBean(Map<String, Object> map, T bean) {
        BeanMap beanMap = BeanMap.create(bean);
        beanMap.putAll(map);
        return bean;
    }

    /**
     * 将List<T>转换为List<Map<String, Object>>
     *
     * @param objList
     * @return
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static <T> List<Map<String, Object>> objectsToMaps(List<T> objList) {
        List<Map<String, Object>> list = Lists.newArrayList();
        if (objList != null && objList.size() > 0) {
            Map<String, Object> map = null;
            T bean = null;
            for (int i = 0, size = objList.size(); i < size; i++) {
                bean = objList.get(i);
                map = beanToMap(bean);
                list.add(map);
            }
        }
        return list;
    }

    /**
     * 将List<Map<String,Object>>转换为List<T>
     *
     * @param maps
     * @param clazz
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static <T> List<T> mapsToObjects(List<Map<String, Object>> maps, Class<T> clazz)
            throws InstantiationException, IllegalAccessException {
        List<T> list = Lists.newArrayList();
        if (maps != null && maps.size() > 0) {
            Map<String, Object> map = null;
            T bean = null;
            for (int i = 0, size = maps.size(); i < size; i++) {
                map = maps.get(i);
                bean = clazz.newInstance();
                mapToBean(map, bean);
                list.add(bean);
            }
        }
        return list;
    }
    
	
	public static void main(String[] args) {
		
		Integer i = null;
		ObjectUtil.isEmpty(i);
	}
	
}
