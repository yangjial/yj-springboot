package com.ft.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.Base64;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
public class FileUtil {

	public static void uploadFile(byte[] file, String filePath, String fileName) throws Exception {
		File targetFile = new File(filePath);
		if (!targetFile.exists()) {
			targetFile.mkdirs();
		}
		FileOutputStream out = new FileOutputStream(filePath + fileName);
		out.write(file);
		out.flush();
		out.close();
	}

	public static boolean deleteFile(String fileName) {
		File file = new File(fileName);
		// 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
		if (file.exists() && file.isFile()) {
			if (file.delete()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static String renameToUUID(String fileName) {
		return UUID.randomUUID() + "." + fileName.substring(fileName.lastIndexOf(".") + 1);
	}
	
	public static String fileMd5(InputStream inputStream) {
		try {
			return DigestUtils.md5Hex(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static String getPath() {
		return "/" + LocalDate.now().toString().replace("-", "/") + "/";
	}

	/**
	 * 将文本写入文件
	 * 
	 * @param value
	 * @param path
	 */
	public static void saveTextFile(String value, String path) {
		FileWriter writer = null;
		try {
			File file = new File(path);
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}

			writer = new FileWriter(file);
			writer.write(value);
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static String getText(String path) {
		File file = new File(path);
		if (!file.exists()) {
			return null;
		}

		try {
			return getText(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static String getText(InputStream inputStream) {
		InputStreamReader isr = null;
		BufferedReader bufferedReader = null;
		try {
			isr = new InputStreamReader(inputStream, "utf-8");
			bufferedReader = new BufferedReader(isr);
			StringBuilder builder = new StringBuilder();
			String string;
			while ((string = bufferedReader.readLine()) != null) {
				string = string + "\n";
				builder.append(string);
			}

			return builder.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (isr != null) {
				try {
					isr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}
	
	/**
	 * 将文件转成base64 字符串
	 * 
	 * @param path文件路径
	 * @return 
	 * @throws Exception *
	 * @throws Exception
	 */

	public static String encodeBase64File(String path) throws Exception {
		File file = new File(path);
		FileInputStream inputFile = null;
		byte[] buffer = null;
		try {
			inputFile = new FileInputStream(file);
			buffer = new byte[inputFile.available()];
			inputFile.read(buffer);
		}finally {
			if(inputFile != null)inputFile.close();
			
		}
		return Base64.getEncoder().encodeToString(buffer);
	}

	/**
	 * 将base64字符解码保存文件
	 * 
	 * @param base64Code
	 * @param targetPath
	 * @throws Exception 
	 * @throws Exception
	 */

	public static void decoderBase64File(String base64Code, String targetPath) throws Exception  {
		byte[] buffer =  Base64.getDecoder().decode(base64Code);
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(targetPath);
			out.write(buffer);
		} finally {
			if(out != null) out.close();
		}
	}
	/**
	 * 将base64字符解码保存文件
	 *
	 * @param base64Code
	 * @param targetPath
	 * @throws Exception
	 * @throws Exception
	 */

	public static void decoderNewBase64File(String base64Code, String targetPath) throws Exception  {
		byte[] buffer =  Base64.getDecoder().decode(base64Code);
		FileOutputStream out = null;
		try {
			if (StringUtils.isNotEmpty(targetPath)) {
				File file = new File(targetPath);

				if (!file.getParentFile().exists()) {

					file.getParentFile().mkdirs();

				}

			}
			out = new FileOutputStream(targetPath);
			out.write(buffer);
		} finally {
			if(out != null) out.close();
		}
	}
	/**
	 * 将base64字符保存文本文件
	 * 
	 * @param base64Code
	 * @param targetPath
	 * @throws Exception 
	 * @throws Exception
	 */

	public static void toFile(String base64Code, String targetPath) throws Exception {

		byte[] buffer = base64Code.getBytes();
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(targetPath);
			out.write(buffer);
		}finally {
			if(out != null)out.close();
		}
	}

	public static void main(String[] args) {
		try {
			String base64Code = encodeBase64File("D:\\eclipseSvnWorkspace\\ty-overrun\\src\\main\\resources\\static\\template\\car3.jpg");
			System.out.println(base64Code);
			//decoderBase64File(base64Code, "D:/2.tif");
			//toFile(base64Code, "D:\\three.txt");
		} catch (Exception e) {
			e.printStackTrace();

		}

	}
}
