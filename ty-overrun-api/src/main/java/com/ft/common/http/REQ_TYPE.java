package com.ft.common.http;

/**
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
public enum REQ_TYPE {
	GET, POST
}
