package com.ft.common.http;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * @author mdc mdc@comit.com.cn maidachao comit -ft-
 */
public class DownloadFileFromURL {

	private static String[] videoSufs = { "mp4", "flv", "avi", "rm", "rmvb", "wmv" };
	private static String[] imageSufs = { "jpg", "tif","png" , "jpeg", "bmp" };

	// 链接url下载图片
	public static String downloadFile(String urlList, String path) throws Exception {
		
		URL url = new URL(urlList);
		File file = new File(path);
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		DataInputStream dataInputStream = null;
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			dataInputStream = new DataInputStream(url.openStream());
			byte[] buffer = new byte[dataInputStream.available()];
			int length;
			while ((length = dataInputStream.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			}
			fileOutputStream.write(output.toByteArray());
		}finally {
			if(dataInputStream != null)dataInputStream.close();
			fileOutputStream.close();
		}
		String name = url.getFile();
		return name.substring(name.lastIndexOf("/") + 1, name.length());
	}

	public boolean isImage(File file) {
		if (!file.exists()) {
			return false;
		}
		BufferedImage image = null;
		try {
			// image = ImageIO.read(file.getInputStream());
			image = ImageIO.read(file);
			if (image == null || image.getWidth() <= 0 || image.getHeight() <= 0) {
				return false;
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 检查视频
	 * @param fileName
	 * @throws Exception
	 */
	public static void videoCheck(String fileName) throws Exception {
		if (fileName == null || "".equals(fileName)) {
			throw new RuntimeException("文件名不能为空！");
		}
		boolean result = false;
		for (String videoSuf : videoSufs) {
			if (fileName.endsWith("." + videoSuf)) {
				result = true;
				break;
			}
		}
		if (!result) {
			 throw new RuntimeException("视频格式不正确 ！");
		}
	}

	/**
	 * 检查图片
	 * @param fileName
	 * @throws Exception
	 */
	public static void imageCheck(String fileName) throws Exception {
		if (fileName == null || "".equals(fileName)) {
			throw new RuntimeException("文件名不能为空！");
		}
		boolean result = false;
		for (String imageSuf : imageSufs) {
			if (fileName.endsWith("." + imageSuf)) {
				result = true;
				break;
			}
		}
		if (!result) {
			 throw new RuntimeException("图片格式不正确 ！");
		}
	}

	public static void main(String[] args) throws Exception {
		String url = "";
		String path = "";
		DownloadFileFromURL.imageCheck("abc.1mp41234");

	}




	/**
	 * 从网络Url中下载文件
	 * @param urlStr
	 * @param fileName
	 * @param savePath
	 */
	public static String  downLoadFromUrl(String urlStr,String fileName,String savePath,int connectTimeout) throws IOException {
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		//设置超时间为6秒
		conn.setConnectTimeout(connectTimeout*1000);
		conn.setReadTimeout(5000);
		//防止屏蔽程序抓取而返回403错误
		conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");

		//得到输入流
		InputStream inputStream = conn.getInputStream();
		//获取自己数组
		byte[] getData = readInputStream(inputStream);

		//文件保存位置
		File saveDir = new File(savePath);
		if(!saveDir.exists()){
			saveDir.mkdir();
		}
		File file = new File(saveDir+File.separator+fileName);
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(getData);
		if(fos!=null){
			fos.close();
		}
		if(inputStream!=null){
			inputStream.close();
		}
		String name = url.getFile();
		return name.substring(name.lastIndexOf("/") + 1, name.length());

	}



	/**
	 * 从输入流中获取字节数组
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	public static  byte[] readInputStream(InputStream inputStream) throws IOException {
		byte[] buffer = new byte[1024];
		int len = 0;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		while((len = inputStream.read(buffer)) != -1) {
			bos.write(buffer, 0, len);
		}
		bos.close();
		return bos.toByteArray();
	}
}