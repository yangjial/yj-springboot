package com.ft.base.web.advice;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
public class LocalStateCode {
	
	/**
	 * 返回结果 执行成功200
	 */
	public final static String RETURN_SUCC = "0200";
	
	/**
	 * 返回结果 没有授权 203
	 */
	public final static String NO_TOKEN = "0203";
	
	/**
	 * 返回结果 授权失败 401
	 */
	public final static String AUTHORIZE_FAIL = "0401";

	/**
	 * 返回结果 数据异常统一返回502
	 */
	public final static String RETURN_DATA_ERROR = "0502";

	/**
	 * 返回结果 数据重复503
	 */
	public final static String RETURN_DOUBLE_ERROR = "0503";

	/**
	 * 返回结果 数据违反长度约束504
	 */
	public final static String RETURN_MAX_ERROR = "0504";

	/**
	 * 返回结果 执行失败500
	 */
	public final static String RETURN_EXE_FAIL= "0500";

	/**
	 * 返回结果 接口无响应
	 */
	public final static String NO_RESPONSE_ERROR = "0408";

	/**
	 * 返回结果 数据为空499
	 */
	public final static String RETURN_NULL_ERROR = "0000";

	public final static Map<String, String> RETURN_MSG_MAP = new HashMap<String, String>();
	static {
		RETURN_MSG_MAP.put(RETURN_SUCC, "数据接口调用成功");
		RETURN_MSG_MAP.put(RETURN_DATA_ERROR, "数据异常");
		RETURN_MSG_MAP.put(RETURN_DOUBLE_ERROR, "数据重复");
		RETURN_MSG_MAP.put(RETURN_NULL_ERROR, "数据字段有空");
		RETURN_MSG_MAP.put(NO_RESPONSE_ERROR, "接口无响应");
		RETURN_MSG_MAP.put(RETURN_EXE_FAIL, "操作异常");
	}
	
}
