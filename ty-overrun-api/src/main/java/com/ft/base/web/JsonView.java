package com.ft.base.web;

import java.util.HashMap;

import com.ft.base.web.advice.LocalStateCode;
import com.ft.common.json.JsonHelper;

/**
 * 
 * 对外的返回结果标准
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
public class JsonView extends HashMap<String, Object> {
	
	private static final long serialVersionUID = 4579317496826442567L;


	public JsonView() {
	}

	public JsonView(String code,String data, String msg) {
		this.put("rscode", code);
		this.put("data", data);
		this.put("rsmsg", msg);
	}


	public JsonView setSuccess(String dataId) {
		put("rscode", LocalStateCode.RETURN_SUCC);
		put("data", "success");
		put("errorCode",0);
		put("rsmsg", LocalStateCode.RETURN_MSG_MAP.get(LocalStateCode.RETURN_SUCC));
		put("dataId", dataId);
		return this;
	}
	public JsonView setSuccess() {
		put("rscode", LocalStateCode.RETURN_SUCC);
		put("data", "success");
		put("errorCode",0);
		put("rsmsg", LocalStateCode.RETURN_MSG_MAP.get(LocalStateCode.RETURN_SUCC));
		return this;
	}

	public JsonView setData(Object obj) {
		put("data",obj);
		return this;
	}

	public JsonView setFail(String errorCode,Object data,String dataId) {
		put("rscode", LocalStateCode.RETURN_EXE_FAIL);
		put("data", data);
		put("errorCode",errorCode);
		put("rsmsg", "failed");
		put("dataId", dataId);
		return this;
	}
    public JsonView setFail(String errorCode,Object data) {
        put("rscode", LocalStateCode.RETURN_EXE_FAIL);
        put("data", data);
        put("errorCode",errorCode);
        put("rsmsg", "failed");
        return this;
    }
	public JsonView addAttribute(String key, Object value) {
		put(key, value);
		return this;
	}

	public Object getArttribute(String key) {
		return get(key);
	}

	public JsonView setReturnMsg(String msg) {
		put("rsmsg", msg);
		return this;
	}

	public String getReturnCode() {
		return (String) get("code");
	}

	public void setReturnCode(String returnCode) {
		if (get("rsmsg") == null) {
			setReturnMsg(LocalStateCode.RETURN_MSG_MAP.get(returnCode));
		}
		put("rscode", returnCode);
	}

	public String getReturnMsg() {
		return (String) get("rsmsg");
	}
	
	@Override
	public String toString() {
		return JsonHelper.parseToJson(this, null);
	}
}
