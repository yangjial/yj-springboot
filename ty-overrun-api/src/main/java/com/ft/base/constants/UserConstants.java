package com.ft.base.constants;

/**
 * 
 * 用户相关常量
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
public interface UserConstants {

	// 加密次数 
	int HASH_ITERATIONS = 3;

	// 登陆用户
	String LOGIN_USER = "login_user";

	//按钮权限
	String USER_PERMISSIONS = "user_permissions";
	
	//菜单权限
	String USER_MENU = "user_menu";

}
