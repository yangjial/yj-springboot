package com.ft.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger配置类
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	public static final String SWAGGER_SCAN_BASE_PACKAGE = "com.ft.modules.feiqiAPI";

	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("通用API接口文档")
				.apiInfo(apiInfo())
				.pathMapping("/")
				.select()
				.apis(RequestHandlerSelectors.basePackage(SWAGGER_SCAN_BASE_PACKAGE))
				.paths(PathSelectors.any()) // 可以根据url路径设置哪些请求加入文档，忽略哪些请求
				.build();

	}


	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("接口文档").description("yj专属")
				.contact(new Contact("", null, null)).version("版本号:1.0").build();
	}
	

	
}
