package com.ft.config;

import java.lang.reflect.Field;
import java.util.List;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.servlet.config.annotation.*;

/**
 * @author mdc mdc@comit.com.cn 
 * maidachao comit 
 * -ft-
 */
@Configuration
public class WebConfiguration extends WebMvcConfigurationSupport {
	/**/
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				.allowedOrigins("*")
				.allowCredentials(true)
				.allowedMethods("*")
				.allowedHeaders("*")
				.maxAge(18000L);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/doc.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
		super.addResourceHandlers(registry);
	}



	@Override
	protected void configureAsyncSupport(AsyncSupportConfigurer configurer) {
		configurer.setTaskExecutor(new ConcurrentTaskExecutor(Executors.newFixedThreadPool(3)));
		configurer.setDefaultTimeout(30000);
	}

	@Override
	protected void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new DateConverterConfig());
	}

}
