package com.ft.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;

/**
 * @author mdc mdc@comit.com.cn
 * maidachao comit
 * -ft-
 */
@Configuration
public class BaseConfig {
	
	//上传路径
	@Value("${base.uploadPath}")
	private String uploadPath;
	
	//下载路径
	@Value("${base.downloadPath}")
	private String downloadPath;

	//下载路径
	@Value("${base.httpUrl}")
	private String httpUrl;
	
	/**
	 * 查看文件路径
	 */
	@Value("${base.uploadPathUrl}")
	private String uploadPathUrl;
	
    public String getUploadPathUrl() {
		return uploadPathUrl;
	}


	public void setUploadPathUrl(String uploadPathUrl) {
		this.uploadPathUrl = uploadPathUrl;
	}


	/**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        // 开启 count 的 join 优化,只针对 left join !!!
        return new PaginationInterceptor().setCountSqlParser(new JsqlParserCountOptimize(true));
    }

	
	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public String getHttpUrl() {
		return httpUrl;
	}

	public void setHttpUrl(String httpUrl) {
		this.httpUrl = httpUrl;
	}
}
