package com.ft.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.googlecode.aviator.AviatorEvaluator;

public class TestAviator {
	
    private String aa="jack";
    
    public String getAa() {
        return aa;
    }
 
    public void setAa(String aa) {
        this.aa = aa;
    }

	public static void main(String[] args) {
		
		TestAviator tm = new TestAviator();
		tm.testAviatorHelloWorld();
		
	}
	
	public void testAviatorHelloWorld() {

        // execute执行方式，需传递Map格式
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("age", "18");
        map.put("num", 18);
        map.put("test", new TestAviator());
        System.out.println(AviatorEvaluator.execute("'His age is '+ age +'!'", map));
        
        System.out.println(AviatorEvaluator.execute("'test.aa ='+test.aa", map));
        
        System.out.println(AviatorEvaluator.execute("num < 2 && num >1",map));
        

    }
	
	public void testAviatorEasy() {

        int[] a = new int[]{6, 7, 8, 9};
        Map<String, Object> env = new HashMap<String, Object>();
        env.put("a", a);
 
        System.out.println(AviatorEvaluator.execute("1 + 2 + 3"));
        System.out.println(AviatorEvaluator.execute("a[1] + 100", env));
        System.out.println(AviatorEvaluator.execute("'a[1]=' + a[1]", env));
        //求数组长度
        System.out.println(AviatorEvaluator.execute("count(a)", env));
        //求数组总和
        System.out.println(AviatorEvaluator.execute("reduce(a, +, 0)", env));
        //检测数组每个元素都在 0 <= e < 10 之间。
        System.out.println(AviatorEvaluator.execute("seq.every(a, seq.and(seq.ge(0), seq.lt(10)))", env));

    }
	
	public void testAviatorAdvanced() {

        Map<String, Object> env = new HashMap<String, Object>();
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(3);
        list.add(20);
        list.add(10);
        env.put("list", list);
        env.put("email", "killme2008@gmail.com");
        env.put("test", new TestAviator());
        env.put("date",new Date());
        String username = (String) AviatorEvaluator.execute("email=~/([\\w0-8]+)@\\w+[\\.\\w+]+/ ? $1 : 'unknow' ", env);
        System.out.println(username);
        System.out.println(AviatorEvaluator.exec("a>0? 'yes':'no'", 1));
        System.out.println(AviatorEvaluator.execute("'test.aa ='+test.aa", env));
        System.out.println(AviatorEvaluator.execute(" obj==nil", env));
        System.out.println(AviatorEvaluator.execute(" date > '2018-05-21 00:00:00:00'", env));
        System.out.println(AviatorEvaluator.execute(" date > '2018-05-22 12:00:00:00'", env));
 
        Object result = AviatorEvaluator.execute("count(list)", env);
        System.out.println(result);  // 3
        result = AviatorEvaluator.execute("reduce(list,+,0)", env);
        System.out.println(result);  // 33
        result = AviatorEvaluator.execute("filter(list,seq.gt(9))", env);
        System.out.println(result);  // [10, 20]
        result = AviatorEvaluator.execute("include(list,10)", env);
        System.out.println(result);  // true
        result = AviatorEvaluator.execute("sort(list)", env);
        System.out.println(result);  // [3, 10, 20]
        AviatorEvaluator.execute("map(list,println)", env);

    }
	
}
