package com.ft.test;

import java.io.Serializable;
import java.util.List;

/**
 * <p>平板称重车辆信息实体对象</p>
 */
public class PbczEntity implements Serializable {

	private static final long serialVersionUID = -2736066408850312585L;
	
	private String dataID;
	private String indexCode;
	private String checkID;
	private String plateNo;
	private int plateColor;
	private int plateType;
	private String vehicleColor;
	private String vehicleType;
	private float vehicleSpeed;
	private int laneNo;
	private int isOverWeight;
	private int axleNum;
	private int isDirect;
	private float overWeight;
	private float vehicleWeight;
	private float limitWeight;
	private String checkTime;
	private String passCode;
	private String axleModel;
	private int dataIsCredible;
	private String drivingDirection;
	private int isDangerous;
	private int tyreNums;
	private String loadRate;
	private int vehicleHeight;
	private int vehicleWidth;
	private int vehicleLong;
	private List<Object> axleArray;
	private List<Object> imageArray;
	private List<Object> videoArray;
	private List<Object> spareArray;

	/**
	 * @return the dataID
	 */
	public String getDataID() {
		return dataID;
	}

	/**
	 * @param dataID
	 *            the dataID to set
	 */
	public void setDataID(String dataID) {
		this.dataID = dataID;
	}

	/**
	 * @return the indexCode
	 */
	public String getIndexCode() {
		return indexCode;
	}

	/**
	 * @param indexCode
	 *            the indexCode to set
	 */
	public void setIndexCode(String indexCode) {
		this.indexCode = indexCode;
	}

	/**
	 * @return the checkID
	 */
	public String getCheckID() {
		return checkID;
	}

	/**
	 * @param checkID
	 *            the checkID to set
	 */
	public void setCheckID(String checkID) {
		this.checkID = checkID;
	}

	/**
	 * @return the plateNo
	 */
	public String getPlateNo() {
		return plateNo;
	}

	/**
	 * @param plateNo
	 *            the plateNo to set
	 */
	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	/**
	 * @return the plateColor
	 */
	public int getPlateColor() {
		return plateColor;
	}

	/**
	 * @param plateColor
	 *            the plateColor to set
	 */
	public void setPlateColor(int plateColor) {
		this.plateColor = plateColor;
	}

	/**
	 * @return the plateType
	 */
	public int getPlateType() {
		return plateType;
	}

	/**
	 * @param plateType
	 *            the plateType to set
	 */
	public void setPlateType(int plateType) {
		this.plateType = plateType;
	}

	/**
	 * @return the vehicleColor
	 */
	public String getVehicleColor() {
		return vehicleColor;
	}

	/**
	 * @param vehicleColor
	 *            the vehicleColor to set
	 */
	public void setVehicleColor(String vehicleColor) {
		this.vehicleColor = vehicleColor;
	}

	/**
	 * @return the vehicleType
	 */
	public String getVehicleType() {
		return vehicleType;
	}

	/**
	 * @param vehicleType
	 *            the vehicleType to set
	 */
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	/**
	 * @return the vehicleSpeed
	 */
	public float getVehicleSpeed() {
		return vehicleSpeed;
	}

	/**
	 * @param vehicleSpeed
	 *            the vehicleSpeed to set
	 */
	public void setVehicleSpeed(float vehicleSpeed) {
		this.vehicleSpeed = vehicleSpeed;
	}

	/**
	 * @return the laneNo
	 */
	public int getLaneNo() {
		return laneNo;
	}

	/**
	 * @param laneNo
	 *            the laneNo to set
	 */
	public void setLaneNo(int laneNo) {
		this.laneNo = laneNo;
	}

	/**
	 * @return the isOverWeight
	 */
	public int getIsOverWeight() {
		return isOverWeight;
	}

	/**
	 * @param isOverWeight
	 *            the isOverWeight to set
	 */
	public void setIsOverWeight(int isOverWeight) {
		this.isOverWeight = isOverWeight;
	}

	/**
	 * @return the axleNum
	 */
	public int getAxleNum() {
		return axleNum;
	}

	/**
	 * @param axleNum
	 *            the axleNum to set
	 */
	public void setAxleNum(int axleNum) {
		this.axleNum = axleNum;
	}

	/**
	 * @return the isDirect
	 */
	public int getIsDirect() {
		return isDirect;
	}

	/**
	 * @param isDirect
	 *            the isDirect to set
	 */
	public void setIsDirect(int isDirect) {
		this.isDirect = isDirect;
	}

	/**
	 * @return the overWeight
	 */
	public float getOverWeight() {
		return overWeight;
	}

	/**
	 * @param overWeight
	 *            the overWeight to set
	 */
	public void setOverWeight(float overWeight) {
		this.overWeight = overWeight;
	}

	/**
	 * @return the vehicleWeight
	 */
	public float getVehicleWeight() {
		return vehicleWeight;
	}

	/**
	 * @param vehicleWeight
	 *            the vehicleWeight to set
	 */
	public void setVehicleWeight(float vehicleWeight) {
		this.vehicleWeight = vehicleWeight;
	}

	/**
	 * @return the limitWeight
	 */
	public float getLimitWeight() {
		return limitWeight;
	}

	/**
	 * @param limitWeight
	 *            the limitWeight to set
	 */
	public void setLimitWeight(float limitWeight) {
		this.limitWeight = limitWeight;
	}

	/**
	 * @return the checkTime
	 */
	public String getCheckTime() {
		return checkTime;
	}

	/**
	 * @param checkTime
	 *            the checkTime to set
	 */
	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}

	/**
	 * @return the passCode
	 */
	public String getPassCode() {
		return passCode;
	}

	/**
	 * @param passCode
	 *            the passCode to set
	 */
	public void setPassCode(String passCode) {
		this.passCode = passCode;
	}

	/**
	 * @return the axleModel
	 */
	public String getAxleModel() {
		return axleModel;
	}

	/**
	 * @param axleModel
	 *            the axleModel to set
	 */
	public void setAxleModel(String axleModel) {
		this.axleModel = axleModel;
	}

	/**
	 * @return the dataIsCredible
	 */
	public int getDataIsCredible() {
		return dataIsCredible;
	}

	/**
	 * @param dataIsCredible
	 *            the dataIsCredible to set
	 */
	public void setDataIsCredible(int dataIsCredible) {
		this.dataIsCredible = dataIsCredible;
	}

	/**
	 * @return the drivingDirection
	 */
	public String getDrivingDirection() {
		return drivingDirection;
	}

	/**
	 * @param drivingDirection
	 *            the drivingDirection to set
	 */
	public void setDrivingDirection(String drivingDirection) {
		this.drivingDirection = drivingDirection;
	}

	/**
	 * @return the isDangerous
	 */
	public int getIsDangerous() {
		return isDangerous;
	}

	/**
	 * @param isDangerous
	 *            the isDangerous to set
	 */
	public void setIsDangerous(int isDangerous) {
		this.isDangerous = isDangerous;
	}

	/**
	 * @return the tyreNums
	 */
	public int getTyreNums() {
		return tyreNums;
	}

	/**
	 * @param tyreNums
	 *            the tyreNums to set
	 */
	public void setTyreNums(int tyreNums) {
		this.tyreNums = tyreNums;
	}

	/**
	 * @return the loadRate
	 */
	public String getLoadRate() {
		return loadRate;
	}

	/**
	 * @param loadRate
	 *            the loadRate to set
	 */
	public void setLoadRate(String loadRate) {
		this.loadRate = loadRate;
	}

	

	/**
	 * @return the axleArray
	 */
	public List<Object> getAxleArray() {
		return axleArray;
	}

	/**
	 * @param axleArray
	 *            the axleArray to set
	 */
	public void setAxleArray(List<Object> axleArray) {
		this.axleArray = axleArray;
	}

	/**
	 * @return the imageArray
	 */
	public List<Object> getImageArray() {
		return imageArray;
	}

	/**
	 * @param imageArray
	 *            the imageArray to set
	 */
	public void setImageArray(List<Object> imageArray) {
		this.imageArray = imageArray;
	}

	/**
	 * @return the videoArray
	 */
	public List<Object> getVideoArray() {
		return videoArray;
	}

	/**
	 * @param videoArray
	 *            the videoArray to set
	 */
	public void setVideoArray(List<Object> videoArray) {
		this.videoArray = videoArray;
	}

	/**
	 * @return the spareArray
	 */
	public List<Object> getSpareArray() {
		return spareArray;
	}

	/**
	 * @param spareArray
	 *            the spareArray to set
	 */
	public void setSpareArray(List<Object> spareArray) {
		this.spareArray = spareArray;
	}

	/**
	 * @return the vehicleHeight
	 */
	public int getVehicleHeight() {
		return vehicleHeight;
	}

	/**
	 * @param vehicleHeight the vehicleHeight to set
	 */
	public void setVehicleHeight(int vehicleHeight) {
		this.vehicleHeight = vehicleHeight;
	}

	/**
	 * @return the vehicleWidth
	 */
	public int getVehicleWidth() {
		return vehicleWidth;
	}

	/**
	 * @param vehicleWidth the vehicleWidth to set
	 */
	public void setVehicleWidth(int vehicleWidth) {
		this.vehicleWidth = vehicleWidth;
	}

	/**
	 * @return the vehicleLong
	 */
	public int getVehicleLong() {
		return vehicleLong;
	}

	/**
	 * @param vehicleLong the vehicleLong to set
	 */
	public void setVehicleLong(int vehicleLong) {
		this.vehicleLong = vehicleLong;
	}
	
	
	
	
}
