package com.ft.test;

import static org.junit.Assert.fail;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

import org.junit.Test;

public class CommonTest {

	@Test
	public void test() {
		fail("Not yet implemented");
	}

	@Test
	public void contextText() {

		// 正确的方式
		String name = "金黄色的sfdf弗兰克的但safd撒酒疯";
		byte[] gbkBytes;
		try {
			gbkBytes = name.getBytes("GBK");//按照对方语境进行编码，语境是GBK，就用GBK
			System.out.println(new String(gbkBytes, "GBK"));//对方拿到解码序列，后进行编码按照约定好的类型
			byte[] utfBytes = name.getBytes("UTF-8");
			System.out.println(new String(utfBytes, "UTF-8"));

//			utf-8编码可以用gbk和iso8859-1解码后编回去
//			gbk编码后只能用iso8859-1解码后编回去
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void a() {
		try {
			System.out.println((double)10/100);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void b() {
		try {
			// 创建简体中文对应的Charset
			Charset charset = Charset.forName("GBK");
			// 获取charset对象对应的编码器
			CharsetEncoder charsetEncoder = charset.newEncoder();
			// 创建一个CharBuffer对象
			CharBuffer charBuffer = CharBuffer.allocate(20);
			charBuffer.put("CSDN-专业IT技术社区");
			charBuffer.flip();
			// 将CharBuffer中的字符序列转换成字节序列
			ByteBuffer byteBuffer = charsetEncoder.encode(charBuffer);
			// 循环访问ByteBuffer中的每个字节
			for (int i = 0; i < byteBuffer.limit(); i++)
			{
				System.out.print(byteBuffer.get(i) + " ");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
