package com.ft.test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.ft.common.utils.FileUtil;

public class TestGetVehicleInfo {

	public static void main(String[] args) {
		TestGetVehicleInfo test=new TestGetVehicleInfo();
		test.getFixedVehicleInfo();
	}

	public void getEqpStatusInfo(){
		 String url ="http://36.2.11.153:8088/eqp/getEqpStatusInfo";
		 String authorizeId = "MDM2MDgyMjA=";
		 String strData = "{\"indexCode\": \"MP5W0\", 	\"deviceState\": \"0\", 	\"exceptionReason\": \"摄像头故障\", 	\"checkTime\": \"2020-01-19 15:15:37\" }";
		 byte[] strData_byte = strData.getBytes(StandardCharsets.UTF_8);
		 String strData_encoder =
		 Base64.getEncoder().encodeToString(strData_byte);

		// 创建HttpClient实例
			CloseableHttpClient httpClient = null;
			// 创建HttpPost实例
			HttpPost httpPost = new HttpPost(url);
			// 装填参数
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("authorizeId", authorizeId)); // 字符串参数 可以循环添加多个
			params.add(new BasicNameValuePair("strData", strData_encoder)); // 字符串参数 可以循环添加多个

			try {
				httpClient = HttpClients.createDefault();
				// 设置请求参数
				httpPost.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
				// 设置header信息
				httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				httpPost.setHeader("User-Agent",
						"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0");
				// 执行请求
				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity resEntity = httpResponse.getEntity();
				// 按指定编码转换结果实体为String类型
				String result = EntityUtils.toString(resEntity, "utf-8");
				System.out.println(result);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (httpClient != null) {
					try {
						httpClient.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
	}

	public void getVehicleInfo(){
		// TODO Auto-generated method stub
				// PbczEntity pbczEntity=new PbczEntity();
				//
				// System.out.println(pbczEntity.getAxleNum());
		String url ="http://36.2.11.152:8087/eqp/getVehicleInfo";
		String authorizeId = "MDM2MDcwM1lXUkM=";//MDM2MDcwM1lXUkM=
				 //String strData = "{\"dataId\": \"360703665067513456\", 	\"indexCode\": \"B2C31\", 	\"checkId\": \"B2C3\", 	\"plateNo\": \"赣A12345\", 	\"plateColor\": 9, 	\"plateType\": 1, 	\"vehicleColor\": \"Z\", 	\"vehicleType\": \"X99\", 	\"vehicleSpeed\": 54, 	\"laneNo\": 2, 	\"isOverWeight\": 1, 	\"axleNum\": 6, 	\"isDirect\": 0, 	\"overWeight\": 10000.0, 	\"vehicleWeight\": 100000.0, 	\"limitWeight\": 49001.0, 	\"checkTime\": \"2019-12-31 14:19:50\", 	\"passCode\": \"229810\", 	\"axleModel\": \"\", 	\"axleArray\": [{ 		\"axleNo\": 0, 		\"axleInfo\": 0, 		\"axleDistance\": 0, 		\"axleSpeed\": 0 	}], 	\"imageArray\": [{ 		\"imageName\": \"null1.jpg\", 		\"imageUrl\": \"http://i2.w.yun.hjfile.cn/doc/201303/54c809bf-1eb2-400b-827f-6f024d7d599b_00.jpg\", 		\"imageBase64\": \"\" 	}], 	\"videoArray\": [{ 		\"videoName\": \"null1.mp4\", 		\"videoUrl\": \"\", 		\"videoBase64\": \"\" 	}], 	\"dataIsCredible\": \"0\", 	\"drivingDirection\": \"1\", 	\"isDangerous\": 0, 	\"tyreNums\": 12, 	\"loadRate\": \"0\", 	\"vehicleHeight\": 0, 	\"vehicleWidth\": 0, 	\"vehicleLong\": 0, 	\"vehicleBrand\": \"\", 	\"lengthoverLimitedRate\": \"0.00\", 	\"widthoverLimitedRate\": \"0.00\", 	\"heightoverLimitedRate\": \"0.00\", 	\"lengthoverLimited\": 0.0, 	\"widthoverLimited\": 0.0, 	\"heightoverLimited\": 0.0 }";
//				 String strData = "{\"dataId\": \"3607036650675113457\", 	\"indexCode\": \"B2C31\", 	\"checkId\": \"B2C3\", 	\"plateNo\": \"赣A12345\", 	\"plateColor\": 9, 	\"plateType\": 1, 	\"vehicleColor\": \"Z\", 	\"vehicleType\": \"X99\", 	\"vehicleSpeed\": 54, 	\"laneNo\": 2, 	\"isOverWeight\": 0, 	\"axleNum\": 6, 	\"isDirect\": 0, 	\"overWeight\": 10100.0, 	\"vehicleWeight\": 100000.0, 	\"limitWeight\": 49001.0, 	\"checkTime\": \"2019-12-31 18:19:50\", 	\"passCode\": \"229810\", 	\"axleModel\": \"\", 	\"axleArray\": [{ 		\"axleNo\": 0, 		\"axleInfo\": 0, 		\"axleDistance\": 0, 		\"axleSpeed\": 0 	}], 	\"imageArray\": [{ 		\"imageName\": \"null1.jpg\", 		\"imageUrl\": \"http://i2.w.yun.hjfile.cn/doc/201303/54c809bf-1eb2-400b-827f-6f024d7d599b_00.jpg\", 		\"imageBase64\": \"\" 	}], 	\"videoArray\": [{ 		\"videoName\": \"null1.mp4\", 		\"videoUrl\": \"\", 		\"videoBase64\": \"\" 	}], 	\"dataIsCredible\": \"0\", 	\"drivingDirection\": \"1\", 	\"isDangerous\": 0, 	\"tyreNums\": 12, 	\"loadRate\": \"0\", 	\"vehicleHeight\": 0, 	\"vehicleWidth\": 0, 	\"vehicleLong\": 0, 	\"vehicleBrand\": \"\", 	\"lengthoverLimitedRate\": \"0.00\", 	\"widthoverLimitedRate\": \"0.00\", 	\"heightoverLimitedRate\": \"0.00\", 	\"lengthoverLimited\": 0.0, 	\"widthoverLimited\": 0.0, 	\"heightoverLimited\": 0.0 }";
				 String strData = "{\"dataId\": \"3607036650675113457\", \"checkBatch\": 1,  \"firstCheckId\": \"0\",	\"checkId\": \"B2C3\", 	\"plateNo\": \"赣A12345\", 	\"plateColor\": 9, 	\"plateType\": 1, 	\"vehicleColor\": \"Z\", 	\"vehicleType\": \"X99\", 	\"vehicleSpeed\": 54,  	\"isOverWeight\": 0, 	\"axleNum\": 6, 	\"overWeight\": 10100.0, 	\"vehicleWeight\": 100000.0, 	\"limitWeight\": 49001.0, 	\"checkTime\": \"2019-12-31 18:19:50\", 	\"axleModel\": \"test\", 	\"axleArray\": [{ 		\"axleNo\": 0, 		\"axleInfo\": 0, 		\"axleDistance\": 0, 		\"axleSpeed\": 0 	}], 	\"imageArray\": [{ 		\"imageName\": \"null1.jpg\", 		\"imageUrl\": \"http://i2.w.yun.hjfile.cn/doc/201303/54c809bf-1eb2-400b-827f-6f024d7d599b_00.jpg\", 		\"imageBase64\": \"\" 	}], 	\"videoArray\": [{ 		\"videoName\": \"null1.mp4\", 		\"videoUrl\": \"\", 		\"videoBase64\": \"\" 	}], 	\"dataIsCredible\": \"0\",  	\"isDangerous\": 0, 	\"tyreNums\": 12, 	\"loadRate\": \"0\", 	\"vehicleHeight\": 0, 	\"vehicleWidth\": 0, 	\"vehicleLong\": 0, 	\"vehicleBrand\": \"车牌品牌测试\", 	\"lengthoverLimitedRate\": \"0.00\", 	\"widthoverLimitedRate\": \"0.00\", 	\"heightoverLimitedRate\": \"0.00\", 	\"lengthoverLimited\": 0.0, 	\"widthoverLimited\": 0.0, 	\"heightoverLimited\": 0.0 }";
//				 String strData = "{\"dataId\": \"360123776966800001\", 	\"indexCode\": \"B2C31\", 	\"checkId\": \"MEBP\", 	\"plateNo\": \"赣A12345\", 	\"plateColor\": 9, 	\"plateType\": 1, 	\"vehicleColor\": \"Z\", 	\"vehicleType\": \"X99\", 	\"vehicleSpeed\": 54, 	\"laneNo\": 2, 	\"isOverWeight\": 1, 	\"axleNum\": 6, 	\"isDirect\": 0, 	\"overWeight\": 1920.0, 	\"vehicleWeight\": 50920.0, 	\"limitWeight\": 49000.0, 	\"checkTime\": \"2019-12-31 14:19:50\", 	\"passCode\": \"229810\", 	\"axleModel\": \"\", 	\"axleArray\": [{ 		\"axleNo\": 0, 		\"axleInfo\": 0, 		\"axleDistance\": 0, 		\"axleSpeed\": 0 	}], 	\"imageArray\": [{ 		\"imageName\": \"null1.jpg\", 		\"imageUrl\": \"\", 		\"imageBase64\": \"iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACkSURBVBhXHY1bDoIwFES7N1ERRSgRl4GIokWC+IjrUkQRCFUSEl3O2PZjcpN75kF+Zxdt7IDvHJThGM9AR+ZpyP0ByPc0B2cWmojitRFwPcJtoeG+FLA7zFSijqYotxMlaZKXtKmNdyIqBayYqZ6PlY48GILw1AJPKerYRiNUhAYyv4+r1wP5HCkKZqBOqIJyU6ZkNekuLpq9jSo21a6slKqYiT+ERHzg/h761wAAAABJRU5ErkJggg==\" 	}], 	\"videoArray\": [{ 		\"videoName\": \"null1.mp4\", 		\"videoUrl\": \"\", 		\"videoBase64\": \"\" 	}], 	\"dataIsCredible\": \"0\", 	\"drivingDirection\": \"1\", 	\"isDangerous\": 0, 	\"tyreNums\": 12, 	\"loadRate\": \"0.03\", 	\"vehicleHeight\": 0, 	\"vehicleWidth\": 0, 	\"vehicleLong\": 0, 	\"vehicleBrand\": \"\", 	\"lengthoverLimitedRate\": \"0.00\", 	\"widthoverLimitedRate\": \"0.00\", 	\"heightoverLimitedRate\": \"0.00\", 	\"lengthoverLimited\": 0.0, 	\"widthoverLimited\": 0.0, 	\"heightoverLimited\": 0.0 }";
				 byte[] strData_byte = strData.getBytes(StandardCharsets.UTF_8);
				 String strData_encoder = Base64.getEncoder().encodeToString(strData_byte);
				//
				// MultiValueMap<String, Object> postParameters =new
				// LinkedMultiValueMap<>();
				// postParameters.add("authorizeId", authorizeId);
				// postParameters.add("strData", strData_encoder);
				//
				// // set headers
				// HttpHeaders headers = new HttpHeaders();
				// headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
				//
				// HttpEntity<MultiValueMap<String, Object>> entity = new
				// HttpEntity<>(postParameters, headers);
				//

				// 创建HttpClient实例
				CloseableHttpClient httpClient = null;
				// 创建HttpPost实例
				HttpPost httpPost = new HttpPost(url);
				// 装填参数
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("authorizeId", authorizeId)); // 字符串参数 可以循环添加多个
				params.add(new BasicNameValuePair("strData", strData_encoder)); // 字符串参数 可以循环添加多个

				try {
					httpClient = HttpClients.createDefault();
					// 设置请求参数
					httpPost.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
					// 设置header信息
					httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
					httpPost.setHeader("User-Agent",
							"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0");
					// 执行请求
					HttpResponse httpResponse = httpClient.execute(httpPost);
					HttpEntity resEntity = httpResponse.getEntity();
					// 按指定编码转换结果实体为String类型
					String result = EntityUtils.toString(resEntity, "utf-8");
					System.out.println("执行完成");
					System.out.println(result);
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (httpClient != null) {
						try {
							httpClient.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
	}

	/**
	 * 固定治超检测数据
	 */
	public void getFixedVehicleInfo(){
		String url ="http://36.2.11.153:8087/eqp/getVehicleInfo";
		String authorizeId = "MDM2MDQyOeaXoA==";
		String strData = "{\"dataId\": \"360429546966741\", \"checkId\": \"6EBJ\", \"checkTime\": \"2020-06-30 15:00:37\", \"plateNo\": \"赣A12345\", \"plateColor\": 1, \"plateType\": 1, \"vehicleColor\": 1, \"vehicleType\": 3, \"axleNum\": 6, \"tyreNums\": 22, \"limitWeight\": 49000, \"vehicleWeight\": 47000, \"isOverWeight\": 0, \"overWeight\": 0, \"loadRate\": 0, \"axleArray\": [{\"axleDistance\": 0, \"axleNo\": 1, \"axleSpeed\": 52.5, \"axleInfo\": 4550}], \"vehicleSpeed\": 17, \"imageArray\": [{\"imageName\": \"照片1\", \"imageUrl\": \"\",\"imageFeature\":\"D\", \"imageBase64\": \"\"}, {\"imageName\": \"照片2\", \"imageUrl\": \"\", \"imageFeature\": \"WD\", \"imageBase64\": \"\"}], \"videoArray\": [{\"videoName\": \"视频1\", \"videoUrl\": \"\", \"videoBase64\": \"\"}, {\"videoName\": \"视频2\", \"videoUrl\": \"\", \"videoBase64\": \"\"}], \"dataIsCredible\": \"0\"}";
		byte[] strData_byte = strData.getBytes(StandardCharsets.UTF_8);
		String strData_encoder = Base64.getEncoder().encodeToString(strData_byte);

		// 创建HttpClient实例
		CloseableHttpClient httpClient = null;
		// 创建HttpPost实例
		HttpPost httpPost = new HttpPost(url);
		// 装填参数
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("authorizeId", authorizeId)); // 字符串参数 可以循环添加多个
		params.add(new BasicNameValuePair("strData", strData_encoder)); // 字符串参数 可以循环添加多个

		try {
			httpClient = HttpClients.createDefault();
			// 设置请求参数
			httpPost.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
			// 设置header信息
			httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			httpPost.setHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0");
			// 执行请求
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity resEntity = httpResponse.getEntity();
			// 按指定编码转换结果实体为String类型
			String result = EntityUtils.toString(resEntity, "utf-8");
			System.out.println("执行完成");
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (httpClient != null) {
				try {
					httpClient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 计重收费站点信息数据
	 */
	public void getTollSatitionInfo(){
		String url ="http://127.0.0.1:8089/fixedWeighChargeSite/getTollSatitionInfo";

		String strData = "{\"siteName\": \"测试收费站点名称\",\"checkId\": \"TEST\"," +
				"\"roadName\": \"测试路段名称\",\"cityNo\": 360100,\"cityName\": \"南昌市\"," +
				"\"areaNo\": 360102,\"areaName\": \"东湖区\"}";
		byte[] strData_byte = strData.getBytes(StandardCharsets.UTF_8);
		String strData_encoder = Base64.getEncoder().encodeToString(strData_byte);

		// 创建HttpClient实例
		CloseableHttpClient httpClient = null;
		// 创建HttpPost实例
		HttpPost httpPost = new HttpPost(url);
		// 装填参数
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("strData", strData_encoder)); // 字符串参数 可以循环添加多个

		try {
			httpClient = HttpClients.createDefault();
			// 设置请求参数
			httpPost.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
			// 设置header信息
			httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			httpPost.setHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0");
			// 执行请求
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity resEntity = httpResponse.getEntity();
			// 按指定编码转换结果实体为String类型
			String result = EntityUtils.toString(resEntity, "utf-8");
			System.out.println("执行完成");
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (httpClient != null) {
				try {
					httpClient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 高速公路入口数据测试
	 */
	public void getEntranceInfo(){
		String url ="http://127.0.0.1:8089/tollStationInfo/getEntranceInfo";
		//String authorizeId = "MDM2MDgyMjA=";//MDM2MDcwM1lXUkM=
		String strData = "{\"dataId\": \"3608227780538713422\",\"siteName\": \"西收费站\"," +
				"\"checkId\": \"TEST\",\"laneNo\": \"车道编号\",\"checkTime\": \"2019-12-31 18:19:50\"," +
				"\"plateNo\": \"赣A12345\",\"plateColor\": 0,\"axleNum\": 1," +
				"\"vehicleType\": 1,\"vehicleWeight\": 100000.0,\"limitWeight\": 49001.0," +
				"\"overWeight\": 1920.0,\"isAdviseReturn\": 0," +
				"\"imageArray\": [{ \"imageName\": \"null1.jpg\",\"imageUrl\": \"http://i2.w.yun.hjfile.cn/doc/201303/54c809bf-1eb2-400b-827f-6f024d7d599b_00.jpg\", 	\"imageBase64\": \"\" 	}]}";
		byte[] strData_byte = strData.getBytes(StandardCharsets.UTF_8);
		String strData_encoder = Base64.getEncoder().encodeToString(strData_byte);

		// 创建HttpClient实例
		CloseableHttpClient httpClient = null;
		// 创建HttpPost实例
		HttpPost httpPost = new HttpPost(url);
		// 装填参数
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		//params.add(new BasicNameValuePair("authorizeId", authorizeId)); // 字符串参数 可以循环添加多个
		params.add(new BasicNameValuePair("strData", strData_encoder)); // 字符串参数 可以循环添加多个

		try {
			httpClient = HttpClients.createDefault();
			// 设置请求参数
			httpPost.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
			// 设置header信息
			httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			httpPost.setHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0");
			// 执行请求
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity resEntity = httpResponse.getEntity();
			// 按指定编码转换结果实体为String类型
			String result = EntityUtils.toString(resEntity, "utf-8");
			System.out.println("执行完成");
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (httpClient != null) {
				try {
					httpClient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * 高速公路出口据测试
	 */
	public void getExitInfo(){
		String url ="http://127.0.0.1:8089/tollStationInfo/getExitInfo";
		//String authorizeId = "MDM2MDgyMjA=";//MDM2MDcwM1lXUkM=
		String strData = "{\"dataId\": \"3608227780538713422\",\"entranceSiteName\": \"西收费站\"," +
				"\"entranceCheckId\": \"TEST\",\"entranceLaneNo\": \"入口车道编号\",\"entranceCheckTime\": \"2019-12-31 18:19:50\"," +
				"\"entranceVehicleWeight\": 100000.0,\"entranceAxleNum\": 6,\"plateNo\": \"赣123456\"," +
				"\"plateColor\": 1,\"vehicleType\": 1,\"exitSiteName\": \"东收费站\"," +
				"\"exitCheckId\": \"ASD2\",\"exitCheckTime\": \"2019-12-31 22:19:22\"," +
				"\"vehicleWeight\": 69000.0, \"axleNum\": 6,\"limitWeight\": 49000.0,\"overWeight\": 20000.0,"+
				"\"imageArray\": [{ \"imageName\": \"null1.jpg\",\"imageUrl\": \"http://i2.w.yun.hjfile.cn/doc/201303/54c809bf-1eb2-400b-827f-6f024d7d599b_00.jpg\", 	\"imageBase64\": \"\" 	}]}";
		byte[] strData_byte = strData.getBytes(StandardCharsets.UTF_8);
		String strData_encoder = Base64.getEncoder().encodeToString(strData_byte);

		// 创建HttpClient实例
		CloseableHttpClient httpClient = null;
		// 创建HttpPost实例
		HttpPost httpPost = new HttpPost(url);
		// 装填参数
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		//params.add(new BasicNameValuePair("authorizeId", authorizeId)); // 字符串参数 可以循环添加多个
		params.add(new BasicNameValuePair("strData", strData_encoder)); // 字符串参数 可以循环添加多个

		try {
			httpClient = HttpClients.createDefault();
			// 设置请求参数
			httpPost.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
			// 设置header信息
			httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			httpPost.setHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0");
			// 执行请求
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity resEntity = httpResponse.getEntity();
			// 按指定编码转换结果实体为String类型
			String result = EntityUtils.toString(resEntity, "utf-8");
			System.out.println("执行完成");
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (httpClient != null) {
				try {
					httpClient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
