package com.ft.test;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class HttpMain {
    public static void main(String[] args) {
        CloseableHttpClient httpClient = null;
        JSONObject js = new JSONObject();
        js.put("account","2029439301");
        js.put("password","26DAB781FF0CD4854AC920B4905231BB");
        HttpPost httpPost = new HttpPost("http://47.107.107.161:80/iot/ft/user/login");
        try {
            httpClient = HttpClients.createDefault();
            // 设置请求参数
            httpPost.setEntity(new StringEntity(js.toJSONString(), "utf-8"));
            // 设置header信息
            httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
            httpPost.setHeader("User-Agent",
                    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0");
            // 执行请求
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity resEntity = httpResponse.getEntity();
            // 按指定编码转换结果实体为String类型
            String result = EntityUtils.toString(resEntity, "utf-8");
            System.out.println(result);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
